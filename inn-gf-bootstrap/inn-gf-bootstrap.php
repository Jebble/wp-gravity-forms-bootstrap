<?php
/*
Plugin Name: Internative Gravity Forms Bootstrap
Plugin URI: https://www.internative.nl
Description: Adds bootstrap rendering support based on GF field sizes
Version: 1.1.2
Author: Internative
Author URI: https://www.internative.nl
Text Domain: inn-gf-bs
Bitbucket Plugin URI: https://bitbucket.org/internative/inn-gravity-forms-bootstrap
*/

// Enqueue very basic styling to make the classes work in essence
function inn_gf_bs_enqueue() {
	wp_enqueue_style( 'gf-bootstrap',  plugin_dir_url( __FILE__ ) . 'inn-gf-bootstrap.css', array(), '1.0.0' );
}
add_action( 'wp_enqueue_scripts', 'inn_gf_bs_enqueue' );

/**
 * Add bootstrap classes to field_inputs
 * @param  [string] $classes classes from the field input
 * @param  [object] $field   GF Field Object
 * @param  [Objct] $form   	 GF Form Object
 * @return [string]			 String containing merged old and new field classes
 */
function inn_gf_add_bootstrap_classes( $classes, $field, $form ) {

	// Turn field classes into array
	$field_classes = explode( ' ', $classes );
	$skip_classes = [ 'gform_validation_container', 'gform_hidden' ];

	// Skip validation container
	if ( count( array_intersect( $skip_classes, $field_classes ) ) != 0 ) {
		return $classes;
	}

	// Get the bootstrap classes
	$bsClasses = apply_filters( 'inn-gf-bs-classes', get_default_classes() );

	// Fixed sizes array size => field type
	$fixedSizes = [
		'name' 			=> 'large',
		'fileupload' 	=> 'large'
	];

	// Set specific fields to certain size and add field type as extra class
	if ( array_key_exists( $field->type, $fixedSizes ) ) {
		$field->size = $fixedSizes[ $field->type ];
		$field_classes[] = 'gfield_type_' . $field->type;
	}

	// Merge bootstrap classes based on field size
	$classes = array_merge( $field_classes, $bsClasses[ $field->size ], [ 'gfield-bs' ] );
	return implode( ' ', $classes );
}
add_filter( 'gform_field_css_class', 'inn_gf_add_bootstrap_classes', 10, 3 );

/**
 * Add bootstrap col wrapper around form submit
 * @param  [string] $button Submit button HTML string
 * @param  [object] $form   GF Form Object
 * @return [string]         Wrapped submit button HTML string
 */
function inn_gf_wrap_submit_button( $button, $form ) {

	$button_wrap = '<div class="col">';
	$button_wrap .= $button;
	$button_wrap .= '</div>';

	return $button_wrap;
}
add_filter( 'gform_submit_button', 'inn_gf_wrap_submit_button', 10, 2 );

/**
 * Wrap validation in bootstrap alert
 * @param  [string] $message Validation message
 * @param  [object] $form    GF Form Object
 * @return [string]          Wrapped validation message
 */
function inn_gf_wrap_validation_message( $message, $form ) {

	$validation = '<div class="col">';
		$validation .= '<div class="alert alert-danger" role="alert">';
			$validation .= $message;
	  	$validation .= '</div>';
	$validation .= '</div>';

	return $validation;
}
add_filter( 'gform_validation_message', 'inn_gf_wrap_validation_message', 10, 2 );

/**
 * Wrap confirmation message in bootstrap col
 * @param  [string] $message Confirmation message
 * @return [string]          Wrapped confirmation message
 */
function inn_gf_wrap_confirmation( $message ) {
	$confirmation = '<div class="col">';
		$confirmation .= $message;
	$confirmation .= '</div>';

	return $confirmation;
}
add_filter( 'gform_confirmation', 'inn_gf_wrap_confirmation', 10, 1 );

/**
 * Setup the default bootstrap form classes based on fields size
 * @return [array] Array containing the bootstrap form classes
 */
function get_default_classes() {
	return [
		'small'	=> [
			'col-12',
			'col-sm-6',
			'col-md-4'
		],
		'medium' => [
			'col-12',
			'col-sm-12',
			'col-md-6'
		],
		'large' => [
			'col-12'
		]
	];
}