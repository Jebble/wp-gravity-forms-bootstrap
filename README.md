# README #

This plugin adds basic Bootstrap 4 support to Gravity Forms based on the chosen 'field size'.

## Gebruik ##
The plugin adds Bootstrap 4 classes to the <li> elements in Gravity Forms.
The default classes are

```
#!php
<?php
'small'	=> [
    'col-12',
    'col-sm-6',
    'col-md-4'
],
'medium' => [
    'col-12',
    'col-sm-12',
    'col-md-6'
],
'large' => [
    'col-12'
]
```

The classes can be changed by the 'inn-gf-bs-classes' filter

```
#!php
<?php
add_filter( 'inn-gf-bs-classes', function( $classes ) {
    return $classes;
} );
```

## Opmerkingen ##
There is minimal CSS added to enable out-of-the-box support with Bootstrap 4.
For now it's neccesary to manually add a negative margin-left and negative margin-right equal to half the $grid-gutter-width on u.gform_fields so that input fields don't get excess paddings.

### In Sass/SCSS **

```
#!sass

ul.gform_fields {
	@each $breakpoint in map-keys($grid-gutter-widths) {
		@include media-breakpoint-up($breakpoint) {
			$gutter: map-get($grid-gutter-widths, $breakpoint);
			margin-left: -($gutter / 2);
			margin-right: -($gutter / 2);
		}
	}
}
```

## Changelog ##

### 1.1.2 ###
* Refactor: Fixed field sizes based on array_key instead of value

### 1.1.1 ###
* Feat: Skip Classes option
* Feat: Fixed Field Sizes option

### 1.1 ###
* Feat: Confirmation message wrapped in bootstrap column
* Feat: Validation message wrapped in bootstrap alert
* Refactor: Replaced `gform_pre_render` filter with `gform_field_css_class` filter